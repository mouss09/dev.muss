<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class ContactController extends Controller
{
    /**
     * @Route("/contact", name="moncontact")
     */
    public function contactAction(Request $request)
    {
        $user = new User();

        // On crée le formulaire
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($request->isXmlHttpRequest()){

            if($form->isSubmitted() && $form->isValid()){

                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                $this->addFlash("notice", "Message envoyé avec succès !");
                return $this->redirectToRoute("moncontact");
            }
        }

        // Générer le html du form créer
        $formView = $form->createView();
        return $this->render('AppBundle:Contact:contact.html.twig', ['form' => $formView]);
    }

}
