<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank(message="Veillez renseigner le nom.")
     *
     * @Assert\Regex(
     *     pattern     = "/^[a-z]+$/i" ,
     *     htmlPattern = "^[a-z]+$",
     *     message = "Nom pas valide !"
     * )
     *
     * @Assert\Length(
     *     min = 3,
     *     minMessage = "Le nom est court"
     * )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=null)
     * @Assert\NotBlank(message="Veillez bien renseigner l'email !")
     * @Assert\Regex(
     *     pattern     = "/^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/" ,
     *     htmlPattern = "^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}+$",
     *     message="L'adresse mail n'est pas valide."
     * )
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text")
     * @Assert\NotBlank(message="Veillez renseigner votre message.")
     *
     * @Assert\Length(
     *     min = 100,
     *     minMessage = "Votre message est trop court ."
     * )
     */
    private $message;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="messageDate", type="datetimetz")
     */
    private $messageDate;

    public function __construct()
    {
        $this->messageDate = new \DateTime("NOW");
    }



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return User
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set messageDate
     *
     * @param \DateTime $messageDate
     *
     * @return User
     */
    public function setMessageDate($messageDate)
    {
        $this->messageDate = $messageDate;

        return $this;
    }

    /**
     * Get messageDate
     *
     * @return \DateTime
     */
    public function getMessageDate()
    {
        return $this->messageDate;
    }
}
