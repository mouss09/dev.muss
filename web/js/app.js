// noinspection JSAnnotator
app = {

    /**  Fonction appelée au chargement du DOM **/
    init: function () {
        console.info("app.init")

        /** Ecoute le bouton submit **/
        var  buttonAction = document.getElementById("buttonInput")
        if (buttonAction){
            buttonAction.addEventListener("click", this.submitform)
        }

    },

    submitform: function (e) {
        console.info("app.submitform")

        e.preventDefault()
        app.Onsubmit()

    },

    Onsubmit: function () {
        console.info("app.Onsubmit")

        var submitAction = document.getElementById("buttonInput")
        if (submitAction){
            var  name = document.querySelector(".inputName")
            var  email = document.querySelector(".inputMail")
            var  message = document.querySelector("textarea")

            // POUR LE CSS SPAN : ERREURS
            var spanName = document.querySelector('.errorName')
            spanName.style.position = 'absolute';
            spanName.style.left = '18em';
            spanName.style.top = '24em';
            spanName.style.width = '45%';

            var spanMail = document.querySelector('.errorMail')
            spanMail.style.position = 'absolute';
            spanMail.style.left = '18em';
            spanMail.style.top = '27.7em';
            spanMail.style.width = '47%';

            var spanMessage = document.querySelector('.errorMessage')
            spanMessage.style.position = 'absolute';
            spanMessage.style.left = '18em';
            spanMessage.style.top = '31.4em';
            spanMessage.style.width = '53%';


            var errors = []
            if (name.validity.valueMissing){
                errors["name"] = document.querySelector(".errorName").textContent = "Le champs nom obligatoire."
                return false
            }else {
                var nomRegExp = /^[a-z]+$/i
                if (name.value !== nomRegExp && name.value.length < 3){
                    errors["name"] = document.querySelector(".errorName").textContent = "Le nom n'est pas valide."
                    return false
                }else {
                    errors["name"] = document.querySelector(".errorName").textContent = " "
                }

            }

            if (email.validity.valueMissing){
                errors["email"] = document.querySelector(".errorMail").textContent = "Le champs email obligatoire."
                return false
            }else {
                var emailRegExp = /^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/
                if (!emailRegExp.test(email.value)){
                    errors["email"] = document.querySelector(".errorMail").textContent = "L'email n'est pas valide."
                    return false
                } else {
                    errors["email"] = document.querySelector(".errorMail").textContent = ""
                }
            }

            if (message.validity.valueMissing){
                errors["message"] = document.querySelector(".errorMessage").textContent = "Le champs message obligatoire."
                return false
            }else {
                if (message.value.length < 100){
                    errors["message"] = document.querySelector(".errorMessage").textContent = "Votre message est court."
                    return false
                } else {
                    errors["message"] = document.querySelector(".errorMessage").textContent = ""
                }
            }


            /**** APPEL AJAX : si, pas d'erreurs ****/
            if (!(errors['name'] && errors['email'] && errors['message'])){
                app.callRequestAction()
            }

        }

    },

    /***      FOR PROGRESS BAR TEST ***/
    progressValue: function() {
        console.info("app.progressValue");

        const elemsProgress = Array.from(document.querySelectorAll('[id^="myProgress"]'));

        for (const elem of elemsProgress) {
            // Ajout d'une propriété "persoIniValue" dans l'objet pour conserver la valeur initiale
            elem.persoIniValue = Number(elem.value);
            elem.value = 0;
            setTimeout(function() {
                increment(elem);
            }, 500);//Attente avant progression
        }

        var increment = function(elem){
            let valueProgress = Number(elem.value);
            if (valueProgress < elem.persoIniValue) {
                elem.value = ++valueProgress;
                elem.parentNode.querySelector('p strong').textContent = valueProgress + " % ";
                setTimeout(function() {
                    increment(elem);
                }, 315); //Vitesse de progression
            }
        }

    },


    callRequestAction: function () {
        console.info('app.callRequestAction')

        var xhr = false
        var form = document.querySelector("#form")
        var button = document.querySelector("#buttonInput")

        xhr = new XMLHttpRequest()
        if (!xhr){
            return false
        }

        xhr.onreadystatechange = function () {
            button.disabled = true
            button.textContent = "Chargement..."

            if (xhr.readyState === 4){
               app.callAjaxAction(xhr)
            }
        }
        xhr.open('POST', 'contact', true)
        xhr.setRequestHeader('X-Requested-With','XMLHttpRequest')
        var  data = new  FormData(form)
        xhr.send(data)
    },

    callAjaxAction: function (xhr) {
        console.info('app.callAjaxAction')

        var form = document.querySelector("#form")
        var button = form.querySelector("#buttonInput")

        if (xhr.readyState === XMLHttpRequest.DONE){

            if (xhr.status === 200){

                var inputs = form.querySelectorAll("input,textarea")
                for(var i = 0; i < inputs.length; i++){
                    inputs[i].value = ''
                    button.disabled = false
                    button.textContent = 'Envoyer'
                }

                var divContainer = document.querySelector('.formContact')
                var div = document.createElement('div')
                div.className = 'flash-notice'
                divContainer.appendChild(div)

                // AJOUT DE STYLE
                div.style.position = 'relative';
                div.style.top = '-24em';

                div.textContent = 'Votre message a été envoyé !'
            }else {
                return false
            }
        }
    }


}

/*** Chargement du DOM ***/
window.onload = app.init()

/*** CHARGEMENT SETINTERVAL ***/
window.addEventListener('load',app.progressValue);